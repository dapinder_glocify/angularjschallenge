describe('AddUserController',function(){
   beforeEach(module('app'));
   var $controller;


    var throwMeAnError = function() {
       throw new Error();
     };

   beforeEach(inject(function(_$controller_, $http, $state, OfficesService, UserFactory){
         $controller = _$controller_;
         this.$http_ = $http;
         this.$state_ = $state;
         this.OfficesService_ = OfficesService;
         this.User_ = UserFactory;
         this.user_types = [
          {
            id: 'agent',
            value: 'Agent'
          }, {
            id: 'manager',
            value: 'Manager'
          }
        ];
   }));
   it('save user return checks if id is NaN ',function(){
   	       var self;
            self = this;
            var result =  this.user_.save(); //$controller.save();
          expect(result.id).not.toBeNaN();
   })
});

